package week09;

public class WordSalad implements Iterable<String>{
    
    private WordNode first;
    private WordNode last;
    
    public WordSalad(){
        this.first = null;
        this.last = null;
    }
    
    public WordSalad(java.util.List<String> words){
        for (String word : words){
            addLast(word);
        }
    }
    
    public void add(String word){
        if (this.first == null){
            this.first = new WordNode(word, null);
            this.last = this.first;
            return;
        }
        WordNode newFirst = new WordNode(word, this.first);
        this.first = newFirst;
    }
    
    public void addLast(String word){
        if (this.first == null){
            add(word);
            return;
        }
        WordNode newLast = new WordNode(word, null);
        this.last.next = newLast;
        this.last = newLast; 
    }
    
    private class WordNode{
        private String word;
        private WordNode next;
                
        private WordNode(String word, WordNode next){
            this.word = word;
            this.next = next;
        }
        
    }
    
    public java.util.Iterator<String> iterator(){
        return new java.util.Iterator<String>(){
            private WordNode current = first;
      
            public boolean hasNext(){
                return current != null;
            }
            
            public String next(){
                String result = current.word;
                current = current.next;
                return result;
            }
            
            public void remove(){
                throw new UnsupportedOperationException();
            }
        };
    }
    
    public String toString(){
        StringBuilder result = new StringBuilder("[");
        WordNode node = first;
        while (node != null){
            result.append(node.word);
            result.append(node.next == null ? "" : ", ");
            node = node.next;
        }
        return result.toString() + "]";
    }
    
    
    
    public WordSalad[] distribute(int k){
        // Initialse the output vector.
        WordSalad[] blocks = new WordSalad[k];
        
        for (int i = 0; i < k; i++){
            blocks[i] = new WordSalad(); // This is because defaults are null.
        }
        // End of initialisation.
        
        int arrayPointer = 0;
        WordNode current = this.first;
        
        while (current != null){
            // Inline arrayPointer++ iterates AFTER the expression.
            blocks[arrayPointer++].addLast(current.word);
            current = current.next;
            
            // If the arrayPointer gets larger than the array, reset it.
            if (arrayPointer > k - 1){
                arrayPointer = 0;
            }
        }
        
        return blocks;
    }
        
    public WordSalad[] chop(int k){
        // Initialse the output vector.
        WordSalad[] blocks = new WordSalad[k];
        
        for (int i = 0; i < k; i++){
            blocks[i] = new WordSalad();
        }
        // End of initialisation.
        
        WordNode current = this.first;
        int wordTotal = 0;
        
        // Count the total number of words.
        while (current != null){
            wordTotal++;
            current = current.next;
        }
        
        current = this.first; // Reset current to beginning of list.
        int extra = wordTotal % k; // Num of blocks that need an extra word.
        int even = wordTotal / k; // Base number of words.
        int currentBlock = 0;
        
        while (current != null){
            // Gives the first blocks the extra words.
            if (currentBlock < extra){
                for (int j = 0; j < even + 1; j++){
                    blocks[currentBlock].addLast(current.word);
                    current = current.next;
                }
            } else{ // Otherwise just the base num of words.
                for (int l = 0; l < even; l++){
                    blocks[currentBlock].addLast(current.word);
                    current = current.next;
                }
            }
            
            currentBlock++;
        }
        
        return blocks;
    }
        
    public WordSalad[] split(int k){
        return null;
    }
        
    public static WordSalad merge(WordSalad[] blocks){
        return null;
    }
        
    public static WordSalad join(WordSalad[] blocks){
        return null;
    }

    public static WordSalad recombine(WordSalad[] blocks, int k){
        return null;
    }
    
}
